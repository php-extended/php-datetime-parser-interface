<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DateTime;

use PhpExtended\Parser\ParserInterface;

/**
 * DateTimeParserInterface interface file.
 * 
 * This parser transforms strings into native datetime objects.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<\DateTimeInterface>
 */
interface DateTimeParserInterface extends ParserInterface
{
	
	// php does not accepts covariant return types between interfaces
	
	/**
	 * Sets the given formats by the given formats for this parser to try.
	 * 
	 * @param array<integer, string> $formats
	 * @return DateTimeParserInterface
	 */
	public function setFormats(array $formats) : DateTimeParserInterface;
	
	/**
	 * Adds a parse format to the existing formats for this parser to try.
	 * 
	 * @param string $format
	 * @return DateTimeParserInterface
	 */
	public function addFormat(string $format) : DateTimeParserInterface;
	
}
